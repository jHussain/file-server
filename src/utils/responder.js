export const successRespond = (res, msg, code, data, payload = {}) => res
  .status(code)
  .json({ success: true, code, msg, data, ...payload })
  .end();

export const errorRespond = (res, msg, code, data = {}) => res
  .status(code)
  .json({ success: false, code, msg, data })
  .end();


export const SHORT_MSG = {
  successful: 'successful',
  unsuccessful: 'unsuccessful',
  url_not_found: 'url_not_found',
  unexpected_error: 'unexpected_error',
  unexpected_server_error: 'unexpected_server_error',
  no_authorization: 'no_authorization',
  has_authorization: 'has_authorization',
  invalid_authorization: 'invalid_authorization',
  unexpected_authorization_error: 'unexpected_authorization_error',
  no_app_secret: 'no_app_secret',
  invalid_app_secret: 'invalid_app_secret',
};
